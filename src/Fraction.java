
public class Fraction {
	
	private int num;
	private int denom;
	
	public Fraction(int num, int denom)
	{
		this.num = num;
		this.denom = denom;
		
	}
	
	public Fraction(int num)
	{
		this.num = num;
		this.denom = 1;
		
	}

	public Fraction()
	{
		this.num = 0;
		this.denom = 1;
		
	}
	
	static Fraction ZERO = new Fraction();
	static Fraction UN = new Fraction(1);
	
	public int get_num()
	{
		return this.num;
	}
	public int get_denom()
	{
		return this.denom;
	}
	
	public double valeur()
	{
		return((double)this.num/(double)this.denom);
	}
	
	
	public double add(Fraction f2)
	{
		//Fraction f3 = new Fraction(1,1);
		int num2;
		int denom2;
		
		if(this.denom == f2.get_denom())
		{
			num2 = this.num+f2.get_num();
			denom2 = this.denom;
		}
		else 
		{
			denom2 = f2.get_denom() * this.denom;
			num2 = this.num*f2.get_denom() + f2.get_num()*this.denom;
		}
		
		return new Fraction(num2,denom2).valeur();
		
	}

	public boolean egal(Fraction f)
	{
		if (this.valeur() == f.valeur())
				return true;
		else return false;
	}
	
	public String toString()
	{
		return(this.num + "/" + this.denom + " = " + this.valeur());
	}
	
	public String comp(Fraction f)
	{
		int num_res;
		int num_res2;
		
		if(this.denom == f.denom)
		{
			if(this.num > f.num)
			{
				return(this.toString() +" est plus grand que " +f.toString());
			}
			
			else return(f.toString()+ " plus grand que " + this.toString());
					
		}
		else if(f.valeur() == this.valeur())
		{
			return("Les deux fractions sont égales");
		}
		else 
		{
			
			num_res = f.num * this.denom;
			num_res2 = this.num * f.denom;
			
		//	denom_res = this.denom * f.denom;
			
			if(num_res2 > num_res)
				{
				return(this.toString() + " est plus grand que " + f.toString());
				}
			else
				return(f.toString()+ " est plus grand que " + this.toString());
		}
		
		
		
	}
	
}
